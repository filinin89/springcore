package org.shop;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * The main Data Initializer util class.
 */
public class DataInitializer implements DisposableBean {

    /** The seller initializer. */
    @Autowired
    private SellerInitializer sellerInitializer; // на момент вызова initData() здесь уже должны быть бины
    
    /** The product initializer. */
    @Autowired
    private ProductInitializer productInitializer;
    
    /** The proposal initializer. */
    @Autowired
    @Qualifier("proposalInitializer")
    private ProposalInitializer proposalInitializer;
    
    /** The user initializer. */
    @Autowired
    private UserInitializer userInitializer;

    /**
     * Inits the data.
     */
    public void initData() { // здесь происходит инициализация всех даных для работы
        sellerInitializer.initSellers();
        userInitializer.initUsers();
        productInitializer.initProducts();
        proposalInitializer.initProposals();
    }

    @Override
    public void destroy() throws Exception { // выполнится перед тем как бин разрушится
        System.out.println("Destroy method called for DataInitializers");
    }
}
