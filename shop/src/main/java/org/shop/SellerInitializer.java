package org.shop;

import java.util.*;

import org.shop.api.SellerService;
import org.shop.data.Seller;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.Resource;

/**
 * The Seller Initializer util class.
 */
public class SellerInitializer implements InitializingBean {

    /** The seller service. */
    @Autowired
    private SellerService sellerService;
    
    /** The seller names. */
    @Autowired // spring найдет бин с нужным именем и запишет его данные сюда
    @Qualifier("sellerNames")
    private Map<Long, String> sellerNames = Collections.emptyMap();




    /**
     * Inits the sellers.
     */
    public void initSellers() {
        List<Seller> sellers = new LinkedList<Seller>();
        
        for (Map.Entry<Long, String> entry : sellerNames.entrySet()) {
            Seller seller = new Seller();
            seller.setId(entry.getKey());
            seller.setName(entry.getValue());
            
            sellers.add(seller);
        }
        
        sellerService.importSellers(sellers);
    }


    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("Initialization method called for SellerInitializer");
    }
}
