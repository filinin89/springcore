package org.shop.spring_configuration;

import org.shop.api.*;
import org.shop.api.impl.*;
import org.shop.repository.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ServiceConfig {



    @Bean
    ItemService itemService(ItemRepository itemRepository){
        return new ItemServiceImpl(itemRepository);
    }

    @Bean
    OrderService orderService(){
        return new OrderServiceImpl();
    }

    @Bean
    ProductService productService(ProductRepository repository){
        return new ProductServiceImpl(repository);
    }

    @Bean
    ProposalService proposalService(ProposalRepository proposalRepository){
        return new ProposalServiceImpl(proposalRepository);
    }

    @Bean
    SellerService sellerService(){
        return new SellerServiceImpl();
    }

    @Bean
    UserService userService(){
        return new UserServiceImpl();
    }





}
