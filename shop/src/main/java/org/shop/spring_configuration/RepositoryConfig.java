package org.shop.spring_configuration;

import org.shop.repository.*;
import org.shop.repository.factory.UserRepositoryFactory;
import org.shop.repository.map.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("order.properties")
public class RepositoryConfig {
    @Autowired
    Environment env; // Interface representing the environment in which the current application is running.


    @Bean
    public ItemRepository itemRepository(){
        return new ItemMapRepository();
    }

    @Bean
    public OrderRepository orderRepository(){
        OrderMapRepository orderMapRepository = new OrderMapRepository();
        orderMapRepository.setSequence(Long.valueOf(env.getProperty("initialSequence")));
        return orderMapRepository;
    }

    @Bean
    public ProductRepository productRepository(){
        return new ProductMapRepository();
    }

    @Bean
    public ProposalRepository proposalRepository(){
        return new ProposalMapRepository();
    }

    @Bean
    public SellerRepository sellerRepository(){
        return new SellerMapRepository();
    }

    @Bean
    public UserRepository userRepository(){
        return new UserRepositoryFactory().createUserRepository();
    }



}
