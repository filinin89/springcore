package org.shop.spring_configuration;

import org.shop.bean_post_processor.MyBeanPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ServiceConfig.class, InitializersConfig.class, RepositoryConfig.class, MyBeanPostProcessor.class})
public class ShopConfig {

}


