package org.shop.spring_configuration;

import org.shop.*;
import org.shop.api.ProductService;
import org.shop.api.UserService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class InitializersConfig {

   @Bean(name="sellerNames")
    public Map<Long, String> sellerNames(){
        Map<Long, String> sellers = new HashMap<>();
        sellers.put(1L, "seller1");
        sellers.put(2L, "seller2");
        sellers.put(3L, "seller3");
        sellers.put(4L, "seller4");
        sellers.put(5L, "seller5");

        return sellers;
    }


    @Bean
    public ProductInitializer productInitializer(ProductService productService){
        return new ProductInitializer(productService);
    }

    @Bean
    @Qualifier("proposalInitializer")
    public ProposalInitializer proposalInitializer(){
        return new ProposalInitializer();
    }

    @Bean
    public SellerInitializer sellerInitializer(){
        return new SellerInitializer();
    }

    @Bean
    public UserInitializer userInitializer(UserService userService){
        return new UserInitializer(userService);
    }

    @Bean(initMethod = "initData")
    public DataInitializer dataInitializer(){
        return new DataInitializer();
    }















}
