package org.shop;


import org.shop.api.*;
import org.shop.data.Proposal;
import org.shop.data.Seller;
import org.shop.data.User;
import org.shop.repository.map.OrderMapRepository;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.shop.spring_configuration.ShopConfig;

import java.util.List;

/**
 * The ShopLauncher class.
 */
public class ShopLauncher {
    
    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        AbstractApplicationContext appContext = new AnnotationConfigApplicationContext(ShopConfig.class);
        appContext.registerShutdownHook();
        System.out.println(appContext.getBean(ProductService.class).getProducts());
        System.out.println(appContext.getBean(UserService.class).getUsers());
        SellerService sellerService;
        System.out.println((sellerService = appContext.getBean(SellerService.class)).getSellers());
        System.out.println(appContext.getBean(ProposalService.class).getProposalsBySeller(sellerService.getSellerById(1L)));

        // создадим order
        Seller seller = appContext.getBean(SellerService.class).getSellerById(1L);
        List<Proposal> proposals = appContext.getBean(ProposalService.class).getProposalsBySeller(seller);
        User user = appContext.getBean(UserService.class).getUserById(1L);
        appContext.getBean(OrderService.class).createOrder(user, proposals.get(0));

        // получим order по присвоенному из properties id
        System.out.println(appContext.getBean(OrderMapRepository.class).getOrderById(8L));







    }
}
